from django.shortcuts import render
from .models import IplDeliveryData, IplMatchData
from django.http import JsonResponse
from django.urls import reverse
from django.db.models.functions import Cast
from django.db.models import Count, Sum, FloatField
# Create your views here.

# Number of matches played per year for all the years in IPL.


def matches_played_per_year(request):
    no_of_matches_every_year = IplMatchData.objects.values('season').annotate(
        no_of_matches_every_year=Count('season')).order_by('season')
    return JsonResponse(list(no_of_matches_every_year), safe=False)

# Number of matches won per team per year in IPL.


def number_matches_win_per_year(request):
    no_of_matches_win_per_year = IplMatchData.objects.values('season', 'winner').annotate(
        no_of_matches_win_per_year=Count('winner')).order_by('season')

    return JsonResponse(list(no_of_matches_win_per_year), safe=False)

# Extra runs conceded per team in the year 2016


def extra_runs_consided_per_team_2016(request):
    extra_runs = IplDeliveryData.objects.filter(
        match_id__season=2016).values('bowling_team').annotate(Sum('extra_runs'))
    return JsonResponse(list(extra_runs), safe=False)


# Top 10 economical bowlers in the year 2015
def top_10_eco_bowler_2015(request):
    top_eco_bowler = IplDeliveryData.objects.filter(
        match_id__season=2015, is_super_over=0).values('bowler').annotate(eco_rate=(Cast(Sum('total_runs')*6.0/Count('total_runs'), FloatField()))).order_by('eco_rate')[:10]

    return JsonResponse(list(top_eco_bowler), safe=False)


################## Graph################

def graph_1(request):
    getPath = reverse(matches_played_per_year)

    return render(request, 'graph/graph1.html', {"urlPath": getPath})


def graph_2(request):
    getPath = reverse(number_matches_win_per_year)

    return render(request, 'graph/graph2.html', {"urlPath": getPath})


def graph_3(request):
    getPath = reverse(extra_runs_consided_per_team_2016)

    return render(request, 'graph/graph3.html', {"urlPath": getPath})


def graph_4(request):

    getPath = reverse(top_10_eco_bowler_2015)

    return render(request, 'graph/graph4.html', {"urlPath": getPath})
