from django.core.management.base import BaseCommand, CommandError
import os
import csv
from users.models import IplDeliveryData, IplMatchData


class Command(BaseCommand):

    def handle(self, *args, **option):
        cur_dir = os.path.dirname(__file__)

        with open(os.path.join(cur_dir, "data/matches.csv"), encoding="utf8") as csv_matches_data:
            matchesData = csv.DictReader(csv_matches_data)
            for data in matchesData:
                IplMatchData.objects.create(id=data['id'],
                                            season=data['season'],
                                            city=data['city'],
                                            date=data['date'],
                                            team1=data['team1'],
                                            team2=data['team2'],
                                            toss_winner=data['toss_winner'],
                                            toss_decision=data['toss_decision'],
                                            result=data['result'],
                                            dl_applied=data['dl_applied'],
                                            winner=data['winner'],
                                            win_by_runs=data['win_by_runs'],
                                            win_by_wickets=data['win_by_wickets'],
                                            venue=data['venue'],
                                            umpire1=data['umpire1'],
                                            umpire2=data['umpire2'],
                                            umpire3=data['umpire3']
                                            )

        with open(os.path.join(cur_dir, "data/deliveries.csv"), encoding="utf8") as csv_deliveries_data:
            deliveries_data = csv.DictReader(csv_deliveries_data)

            for data in deliveries_data:
                IplDeliveryData.objects.create(match_id_id=data['match_id'],
                                               inning=data['inning'],
                                               batting_team=data['batting_team'],
                                               bowling_team=data['bowling_team'],
                                               over=data['over'],
                                               ball=data['ball'],
                                               batsman=data['batsman'],
                                               non_striker=data['non_striker'],
                                               bowler=data['bowler'],
                                               is_super_over=data['is_super_over'],
                                               wide_runs=data['wide_runs'],
                                               bye_runs=data['bye_runs'],
                                               legbye_runs=data['legbye_runs'],
                                               noball_runs=data['noball_runs'],
                                               penalty_runs=data['penalty_runs'],
                                               batsman_runs=data['batsman_runs'],
                                               extra_runs=data['extra_runs'],
                                               total_runs=data['total_runs'],
                                               player_dismissed=data['player_dismissed'],
                                               dismissal_kind=data['dismissal_kind'],
                                               fielder=data['fielder'],
                                               )
