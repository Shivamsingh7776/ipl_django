from django.contrib import admin
from django.urls import path
from .views import matches_played_per_year, number_matches_win_per_year, extra_runs_consided_per_team_2016, top_10_eco_bowler_2015, graph_1,graph_2,graph_3, graph_4
urlpatterns = [
    path('answer1/', matches_played_per_year, name='problem-1'),
    path('answer2/', number_matches_win_per_year, name='problem-2'),
    path('answer3/', extra_runs_consided_per_team_2016, name='Problem-3'),
    path('answer4/', top_10_eco_bowler_2015, name='Problem-4'),
    path('graph1/', graph_1, name="Graph-1"),
    path('graph2/', graph_2, name='Graph-2'),
    path('graph3/', graph_3, name="graph-3"),
    path('graph4/', graph_4, name="Graph-4")
]
