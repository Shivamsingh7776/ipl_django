from django.contrib import admin

from .models import IplDeliveryData, IplMatchData
# Register your models here.
admin.site.register(IplDeliveryData)
admin.site.register(IplMatchData)
